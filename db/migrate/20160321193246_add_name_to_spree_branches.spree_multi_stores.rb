# This migration comes from spree_multi_stores (originally 20151222070313)
class AddNameToSpreeBranches < ActiveRecord::Migration
  def change
    add_column :spree_branches, :name, :string
  end
end
