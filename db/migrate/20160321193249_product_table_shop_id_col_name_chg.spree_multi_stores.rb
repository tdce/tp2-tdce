# This migration comes from spree_multi_stores (originally 20151224130507)
class ProductTableShopIdColNameChg < ActiveRecord::Migration
  def change
    rename_column :spree_products, :spree_shop_id, :shop_id
  end
end
