# This migration comes from spree_multi_stores (originally 20151209181701)
class BranchTableShopIdColNameChg < ActiveRecord::Migration
  def change 
     rename_column :spree_branches, :spree_shop_id, :shop_id
  end
end
