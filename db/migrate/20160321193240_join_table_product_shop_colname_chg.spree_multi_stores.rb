# This migration comes from spree_multi_stores (originally 20151207210850)
class JoinTableProductShopColnameChg < ActiveRecord::Migration
  def change
    rename_column :spree_product_shops, :spree_product_id, :product_id
    rename_column :spree_product_shops, :spree_shop_id, :shop_id
  end
end
