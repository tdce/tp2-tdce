# This migration comes from spree_multi_stores (originally 20151224131026)
class AddAllBranchesToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :all_branches, :integer, default: 2
  end
end
