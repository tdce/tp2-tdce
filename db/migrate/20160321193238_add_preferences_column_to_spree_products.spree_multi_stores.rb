# This migration comes from spree_multi_stores (originally 20151206071651)
class AddPreferencesColumnToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :preferences, :text
  end
end
