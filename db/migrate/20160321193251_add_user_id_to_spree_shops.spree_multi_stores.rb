# This migration comes from spree_multi_stores (originally 20160105063332)
class AddUserIdToSpreeShops < ActiveRecord::Migration
  def change
    add_column :spree_shops, :user_id, :integer
    add_index :spree_shops, :user_id
  end
end
