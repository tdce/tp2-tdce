# This migration comes from spree_multi_stores (originally 20151222102506)
class AddDeletedAtToSpreeBranches < ActiveRecord::Migration
  def change
    add_column :spree_branches, :deleted_at, :datetime
  end
end
