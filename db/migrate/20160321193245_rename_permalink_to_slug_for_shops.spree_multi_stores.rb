# This migration comes from spree_multi_stores (originally 20151221182210)
class RenamePermalinkToSlugForShops < ActiveRecord::Migration
  def change
    rename_column :spree_shops, :permalink, :slug
  end
end
