# This migration comes from spree_multi_stores (originally 20151207223739)
class JoinTableBranchProductColnameChg < ActiveRecord::Migration
  def change
    rename_column :spree_branch_products, :spree_branch_id, :branch_id
    rename_column :spree_branch_products, :spree_product_id, :product_id

  end
end
