## Spree mutli-store

## Prerequisites

First, after clone, you should run the following commands :

```ruby
bundle install
rails g spree_multi_stores:install
rake db:seed
```

## Start application

To run the application run :

```ruby
rails s
```